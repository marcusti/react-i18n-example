import React, { useState } from 'react'

const dateTimeOptions = {
  weekday: 'long',
  year: 'numeric',
  month: 'long',
  day: 'numeric',
}

const locales = [
  { code: 'en-US', name: 'English (US)' },
  { code: 'en-GB', name: 'English (GB)' },
  { code: 'de-DE', name: 'Deutsch' },
  { code: 'hi-IN', name: ' हिन्दी' },
  { code: 'ja-JP', name: '日本語' },
  { code: 'zh-Hans', name: '汉语' },
  { code: 'zh-Hant', name: '漢語' },
]

const numbers = [100, 1000, 10000.01, 9.99]

function App() {
  const [locale, setLocale] = useState(locales[0].code)
  const activeClass = 'bg-info bg-gradient text-white'
  const date = new Date()

  return (
    <div className="container">
      <nav className="nav my-2">
        {locales.map((l) => (
          <a
            key={l.code}
            href={`#${l.code}`}
            className={`nav-link ${l.code === locale && activeClass}`}
            onClick={() => setLocale(l.code)}
          >
            {l.name}
          </a>
        ))}
      </nav>

      <h5>{date.toLocaleTimeString(locale, dateTimeOptions)}</h5>

      <h6>{date.toLocaleDateString(locale)}</h6>

      {numbers.map((n, i) => (
        <div key={i}>{n.toLocaleString(locale)}</div>
      ))}
    </div>
  )
}

export default App
